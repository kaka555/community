# Container

### 工作目标和范围

- 负责容器相关软件包的规划、维护和升级
- 负责容器相关技术鞠策
- 及时响应用户反馈，解决相关问题


# 组织会议

- 每双周周二下午4:30-6:00
- 通过邮件申报议题

PS: Container SIG例会与iSulad例会合并


# 成员

- lifeng2221dd1[@lifeng2221dd1](https://gitee.com/lifeng2221dd1)
- caihaomin[@caihaomin](https://gitee.com/caihaomin)
- duguhaotian[@duguhaotian](https://gitee.com/duguhaotian)
- jing-rui[@jing-rui](https://gitee.com/jing-rui)
- flyflyflypeng[@flyflyflypeng](https://gitee.com/flyflyflypeng)

### Maintainer列表

- lifeng2221dd1[@lifeng2221dd1](https://gitee.com/lifeng2221dd1)
- caihaomin[@caihaomin](https://gitee.com/caihaomin)
- duguhaotian[@duguhaotian](https://gitee.com/duguhaotian)
- jing-rui[@jing-rui](https://gitee.com/jing-rui)
- flyflyflypeng[@flyflyflypeng](https://gitee.com/flyflyflypeng)



### Committer列表

- lifeng2221dd1[@lifeng2221dd1](https://gitee.com/lifeng2221dd1)
- caihaomin[@caihaomin](https://gitee.com/caihaomin)
- duguhaotian[@duguhaotian](https://gitee.com/duguhaotian)
- jing-rui[@jing-rui](https://gitee.com/jing-rui)
- flyflyflypeng[@flyflyflypeng](https://gitee.com/flyflyflypeng)


# 联系方式
*邮件列表与isula sig合并*

- [邮件列表](isulad@openeuler.org)


# 项目清单

repository地址：

- src-openeuler/jboss-parent
- src-openeuler/clibcni
- src-openeuler/oci-systemd-hook
- src-openeuler/runc
- src-openeuler/virt-what
- src-openeuler/containerd
- src-openeuler/iSulad-img
- src-openeuler/lcr
- src-openeuler/docker-anaconda-addon
- src-openeuler/protobuf
- src-openeuler/iSulad
- src-openeuler/podman
- src-openeuler/skopeo
- src-openeuler/kata-shim
- src-openeuler/kata_integration
- src-openeuler/kata-micro-kernel
- src-openeuler/busybox
- src-openeuler/container-selinux
- src-openeuler/gobject-introspection
- src-openeuler/lxc
- src-openeuler/docker
- src-openeuler/kata-runtime
- src-openeuler/libevhtp
- src-openeuler/kata-proxy
- src-openeuler/lxcfs
- src-openeuler/containernetworking-plugins
- src-openeuler/libcgroup
- src-openeuler/kata-agent
- src-openeuler/libnetwork
